import cx from 'classnames';
import React from 'react';
import PropTypes from 'prop-types';

import '../scss/components/Button.scss';

export const ButtonIcon = (props) => {
  const {
    alt,
    direction,
    size,
    src,
  } = props;

  const buttonIconClasses = cx({
    button__icon: true,
    'button__icon--small': size === 'small',
    'button__icon--left': direction === 'left',
    'button__icon--right': direction === 'right',
  });

  return (
    <img src={src} alt={alt} className={buttonIconClasses} />
  );
};

ButtonIcon.propTypes = {
  alt: PropTypes.string.isRequired,
  direction: PropTypes.oneOf(['left', 'right']),
  size: PropTypes.string,
  src: PropTypes.string.isRequired,
};

ButtonIcon.defaultProps = {
  direction: 'right',
  size: 'large',
};

const Button = (props) => {
  const {
    as,
    href,
    buttonType,
    children,
    disabled,
    fullWidth,
    type,
  } = props;

  const buttonClasses = cx({
    button: true,
    'button--loud': buttonType === 'loud',
    'button--medium': buttonType === 'medium',
    'button--soft': buttonType === 'soft',
    'button--disabled': disabled,
    'button--full-width': fullWidth,
  });

  const Component = as;

  return (
    <Component href={href} type={type} className={buttonClasses} disabled={disabled}>
      {children}
    </Component>
  );
};

Button.propTypes = {
  as: PropTypes.oneOf(['a', 'button']),
  buttonType: PropTypes.string,
  children: PropTypes.node.isRequired,
  disabled: PropTypes.bool,
  fullWidth: PropTypes.bool,
  href: PropTypes.string,
  type: PropTypes.oneOf(['submit', 'button']),
};

Button.defaultProps = {
  as: 'button',
  buttonType: 'soft',
  disabled: false,
  fullWidth: false,
  href: null,
  type: 'button',
};

Button.Icon = ButtonIcon;

export default Button;
