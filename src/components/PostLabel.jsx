import cx from 'classnames';
import React from 'react';
import PropTypes from 'prop-types';

import '../scss/components/PostLabel.scss';

const PostLabelIcon = (props) => {
  const {
    alt,
    src,
  } = props;

  return (
    <img src={src} alt={alt} className="post-label__icon" />
  );
};

PostLabelIcon.propTypes = {
  alt: PropTypes.string.isRequired,
  src: PropTypes.string.isRequired,
};


const PostLabel = (props) => {
  const {
    children,
    type,
  } = props;

  const postLabelClasses = cx({
    'post-label': true,
    'post-label--loud': type === 'loud',
    'post-label--medium': type === 'medium',
    'post-label--sponsored': type === 'sponsored',
  });

  return (
    <span className={postLabelClasses}>
      {children}
    </span>
  );
};

PostLabel.propTypes = {
  children: PropTypes.node.isRequired,
  type: PropTypes.oneOf(['loud', 'medium', 'sponsored']).isRequired,
};

PostLabel.Icon = PostLabelIcon;

export default PostLabel;
