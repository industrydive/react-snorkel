import React from 'react';

import Label from '../src/components/Label';

import '../src/scss/stories/stories.scss';

export default {
  component: Label,
  title: 'Label',
};

export const LoudLabel = () => (
  <Label type="loud">Brief</Label>
);

export const MediumLabel = () => (
  <Label type="medium">Opinion</Label>
);

export const SoftLabel = () => (
  <Label type="soft">Playbook</Label>
);

export const SponsoredLabel = () => (
  <Label type="sponsored">Sponsored</Label>
);

export const LabelWithIcon = () => (
  <Label type="loud">
    <Label.Icon
      src="https://www.utilitydive.com//static/img/components/labels/podcasts/podcast_red.svg"
      alt="podcast icon"
    />
    {' '}
    Podcast
  </Label>
);

export const SecondaryLabel = () => (
  <Label secondary type="regular">March 21</Label>
);

export const LoudSecondaryLabel = () => (
  <Label secondary type="loud">UPDATED: March 21, 2019 at 11:06 a.m.</Label>
);
